#include "matrix.h"
#include <stdlib.h>

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                           */
/*                      matrix.c                                             */
/*                                                                           */
/*   File that contains the implementation of all the function declared in   */
/*   matrix.h                                                                */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// Creates the matrix
struct Matrix * createMatrix(int inNbLines, int inNbColumns)
{
    struct Matrix * outMatrix = malloc(sizeof(struct Matrix));

    if(outMatrix)
    {
        outMatrix->nbLines      = inNbLines;
        outMatrix->nbColumns    = inNbColumns;

        // Creation of the first array
        outMatrix->matrix = malloc(inNbLines * sizeof(int *));

        if(outMatrix->matrix)
        {   
            // Creation of the subarrays if the first worked
            for(int i = 0 ; i < inNbLines ; i++)
            {
                outMatrix->matrix[i] = malloc(inNbColumns * sizeof(int));
                if(outMatrix->matrix[i])
                {
                    for(int j = 0; j < inNbColumns; j++)
                    {
                        outMatrix->matrix[i][j] = 0;
                    }
                }
                else // if it fails, we delete everything
                {
                    deleteMatrix(outMatrix);
                    return NULL;
                }
            }
        }
        else // Freeing the base element if the array creation fails
        {
            free(outMatrix);
        }
    }

    return outMatrix;
}

// Deletes the matrix
void deleteMatrix(struct Matrix * inMatrix)
{
    if(inMatrix)
    {
        if(inMatrix->matrix)
        {
            for(int i = 0 ; i < inMatrix->nbLines; i++)
            {
                if(inMatrix->matrix[i])
                {
                    free(inMatrix->matrix[i]);
                }
            }
            free(inMatrix->matrix);
        }
        free(inMatrix);
    }
}

// prints the matrix in the console
void displayMatrix(struct Matrix * inMatrix)
{
    int i,j;
    for(i = 0 ; i < inMatrix->nbLines ; i++ )
    {
        for(j = 0;  j < inMatrix->nbColumns ; j++)
        {
            printf("%d ", inMatrix->matrix[i][j]);
        }
        printf("\n");
    }
}


