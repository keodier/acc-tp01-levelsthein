#include "matrix.h"
#include "levensthein.h"



/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                           */
/*                      Main file                                            */
/*                                                                           */
/*   To run the program, it is mandatory to provide to words or sentences    */
/*   as arguments, or the program won't run                                  */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int main(int argc, char ** argv)
{
    if(argc > 2)
    {
        char * firstWord    = argv[1];
        char * secondWord   = argv[2];

        int test = levensthein(firstWord, secondWord);

        printf("Distance is : %d\n", test);
        return 0;
    }
    else
    {
        printf("Please provide two words in arguments\n");

        return -1;
    }
}



