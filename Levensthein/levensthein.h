#ifndef AAC_TP02_LEVENSTHEIN
#define AAC_TP02_LEVENSTHEIN

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "matrix.h"

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                           */
/*                      levensthein                                          */
/*                                                                           */
/*   File that contains the structure and the declaration of the function    */
/*   that are used and related to levensthein calculation in the program     */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                           */
/*                      levensthein                                          */
/*                                                                           */
/*   returns the distance bewteen the two strings provided in argument       */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int levensthein(char *, char *);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                           */
/*                      initDMatrix                                          */
/*                                                                           */
/*   initialize the matrix in order to have the first line and the first     */
/*   column filled bith with 1,2,...,n and the other number with zero        */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
void initDMatrix(struct Matrix *);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                           */
/*                      minTree                                              */
/*                                                                           */
/*   returns the minimum value between tree numbers                          */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
int minTree(int inVal1, int inVal2, int inVal3);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                           */
/*                      displayHistory                                       */
/*                                                                           */
/*   calls displayHistoryRec with the correct inputs                         */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
void displayHistory(struct Matrix * , char * , char * , int );


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                           */
/*                      displayHistoryRec                                    */
/*                                                                           */
/*   goes through the matrix D and recreated the different possibilities to  */
/*   transform the first string into the second                              */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
void displayHistoryRec(struct Matrix * , char * , char * , 
                        int , int , int , int  , char * );

#endif