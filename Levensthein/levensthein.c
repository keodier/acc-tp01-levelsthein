#include "levensthein.h"

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                           */
/*                      levensthein.c                                        */
/*                                                                           */
/*   File that contains the implementation of all the function declared in   */
/*   levensthein.h                                                           */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// Main levensthein function
int levensthein(char * inStr1, char * inStr2)
{
    int str1Len = strlen(inStr1);
    int str2Len = strlen(inStr2);

    struct Matrix * D = createMatrix(str1Len + 1, str2Len + 1);

    initDMatrix(D);

    int i,j, substitution, distance;


    for( i = 1 ; i < str1Len + 1; i++ )
    {

        for(j = 1 ; j < str2Len + 1 ; j++)
        {
            substitution = 1 - (inStr1[i-1] == inStr2[j-1]);
            
            D->matrix[i][j] = minTree(
               D->matrix[i-1][j] + 1,               // Suppression
               D->matrix[i][j-1] + 1,               // Insertion
               D->matrix[i-1][j-1] + substitution   // Substitution
            );
        }
    }

    distance = D->matrix[str1Len][str2Len];

    displayHistory(D, inStr1, inStr2, distance);

    deleteMatrix(D);
    return distance;
}


// Inits the matrix that stores the distance
void initDMatrix(struct Matrix * inMatrix)
{
    int i;

    for(i = 0; i < inMatrix->nbLines; i++)
    {
        inMatrix->matrix[i][0] = i;
    }

    for(i = 0; i < inMatrix->nbColumns; i++)
    {
        inMatrix->matrix[0][i] = i;
    }
}


// Returns the minimum value between tree numbers
int minTree(int inVal1, int inVal2, int inVal3)
{
    int outValue = inVal3;
    
    if (inVal2 < inVal3)
    {
        if(inVal1 < inVal2)
        {
            outValue = inVal1;
        }
        else
        {
            outValue = inVal2;
        }
    }
    else
    {
        if(inVal1 < inVal3)
        {
            outValue = inVal1;
        }
    }
    return outValue;
}


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                           */
/*                      Function to display history                          */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
void displayHistory(struct Matrix * inMatrix, char * inStr1, char * inStr2, 
                    int inMaxEtapes)
{
    displayHistoryRec(inMatrix, inStr1, inStr2, inMaxEtapes, 
                        strlen(inStr1), strlen(inStr2), 0, "");
}



// Recursive function
void displayHistoryRec(struct Matrix * inMatrix, char * inStr1, char * inStr2, 
            int inMaxEtapes, int i, int j, int inNbEtapes , char * inChemin)
{
    if(i > 0 && j > 0)
    {

        int delValue = inMatrix->matrix[i-1][j];    // Upper cell : deletion
        int insValue = inMatrix->matrix[i][j-1];    // Left cell : insertion
        int subValue = inMatrix->matrix[i-1][j-1];  // Upper-left cell : substitution

        // To avoid many tests : finding the minimum then comparing with each
        // cell value
        int minValue = minTree(delValue, insValue, subValue);


        // Reading left case and calling rec function if min
        if(insValue == minValue)
        {
            char * tempIns = malloc((strlen(inChemin) + 32) * sizeof(char));
            sprintf(tempIns, " - Insertion of '%c'\n%s", inStr2[j-1], inChemin);
            displayHistoryRec(inMatrix, inStr1, inStr2, inMaxEtapes, i, j-1 , inNbEtapes+1, tempIns);
            free(tempIns);
        }

        // Reading upper left cell : substitution then rec call
        if(subValue == minValue)
        {
            if(inStr1[i-1] == inStr2[j-1]) // If equity, then there are no changes
            {
                char * tempSub = malloc((strlen(inChemin) + 32) * sizeof(char));;
                sprintf(tempSub, " - Letter '%c' untouched\n%s", inStr1[i-1] ,inChemin);
                displayHistoryRec(inMatrix, inStr1, inStr2, inMaxEtapes, i-1, j-1 , inNbEtapes, tempSub);
                free(tempSub);
            }
            else    // else, there is a substitution
            {
                char * tempSub = malloc((strlen(inChemin) + 32) * sizeof(char));;
                sprintf(tempSub, " - Substitution of '%c' by %c\n%s", inStr1[i-1], inStr2[j-1] ,inChemin);
                displayHistoryRec(inMatrix, inStr1, inStr2, inMaxEtapes, i-1, j-1 , inNbEtapes + 1, tempSub);
                free(tempSub);
            }
        }

        // reading upper cell : deletion
        if(delValue == minValue)
        {
            char * tempDel = malloc((strlen(inChemin) + 32) * sizeof(char));
            sprintf(tempDel, " - Suppresssion of '%c'\n%s", inStr1[i-1], inChemin);
            displayHistoryRec(inMatrix, inStr1, inStr2, inMaxEtapes, i - 1, j, inNbEtapes + 1, tempDel);
            free(tempDel);
        }

    }
    else    // on arrive à la fin du parcours
    {
        if(i == 0 && j == 0)    // Si on n'est pas en (0:0), la  
        {
            if(inNbEtapes == inMaxEtapes)
            {
                printf("Optimal solution : \n%s\n", inChemin);
            }
            else
            {
                printf("Possible solution : \n%s\n", inChemin);
            }
        }        
    }
}