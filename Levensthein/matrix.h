#ifndef AAC_TP02_MATRIX
#define AAC_TP02_MATRIX

#include <stdio.h>

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                           */
/*                      matrix.h                                             */
/*                                                                           */
/*   File that contains the structure and the declaration of the function    */
/*   that are used and related to matrix in the program                      */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                           */
/*                      struct Matrix                                        */
/*                                                                           */
/*   a two dimension array with it height and width                          */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
struct Matrix
{
    int     nbLines;
    int     nbColumns;
    int **  matrix;
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                           */
/*                      createMatrix                                         */
/*                                                                           */
/*   creates a matrix whoose size is provided in parameters and fill it with */
/*   zeros                                                                   */
/*                                                                           */
/*   returns the address of the item or NULL if the creation failed          */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
struct Matrix * createMatrix(int, int);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                           */
/*                      deleteMatrix                                         */
/*                                                                           */
/*   destroys a struct Matrix element                                        */
/*   works even if the matrix is not complete                                */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
void            deleteMatrix(struct Matrix *);



/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                                                                           */
/*                      displayMatrix                                        */
/*                                                                           */
/*   prints the content of a struct Matrix in the console                    */
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
void            displayMatrix(struct Matrix * inMatrix);


#endif